import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

class Fonts {
  static TextStyle robotoRegular({double size = 12.0}) {
    return GoogleFonts.roboto(
      color: WHITE,
      fontSize: size,
    );
  }
}