import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'colors.dart';
import 'font_styles.dart';
import 'redux/app_state.dart';
import 'redux/loader_viewmodel.dart';

class LoaderWidget extends StatelessWidget {
  final Widget child;

  LoaderWidget({@required this.child});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HomePageViewModel>(
        converter: HomePageViewModel.fromStore,
        builder: (BuildContext context, HomePageViewModel viewModel) {
          return viewModel.showLoader ? Material(
            child: Stack(
              children: <Widget>[
                child,
                Container(
                  color: BLACK.withOpacity(0.7),
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/loader.gif',
                        height: 65.0,
                        width: 65.0,
                      ),
                      SizedBox(height: 16.0),
                      Text(
                        'loading...',
                        style: Fonts.robotoRegular(size: 16.0),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ) : child;
        });
  }
}
