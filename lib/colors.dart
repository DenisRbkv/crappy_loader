import 'package:flutter/material.dart';

const Color BG_COLOR = const Color(0xFF222222);
const Color PRIMARY_COLOR = const Color(0xFF222222);
const Color PRIMARY_WHITE = const Color(0xFFEEF0F5);

const Color WHITE = const Color(0xFFFFFFFF);
const Color BLACK = const Color(0xFF000000);
const Color BLACK_TWO = const Color(0xFF060606);

const Color SELECTED_ITEM_COLOR = const Color(0xFFFF9D56);
const Color NOT_SELECTED_ITEM_COLOR = const Color(0xFF222222);

const Color PASTEL_RED = const Color(0xFFeb5757);
const Color WHEAT = const Color(0xFFffde81);
const Color MARIGOLD = const Color(0xFFffbd00);
const Color BLACK_25 = const Color(0x40000000);
const Color WHEAT_20 = const Color(0x33ffde81);
const Color RED = const Color(0xFFff0000);
const Color OCRE = const Color(0xFFc79300);