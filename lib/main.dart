import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'loader_widget.dart';
import 'redux/app_state.dart';
import 'redux/loader_viewmodel.dart';

void main() {
  Store store = Store<AppState>(
    AppState.reducer,
    initialState: AppState.initial(),
  );

  runApp(MyApp(store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp(this.store);

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HomePageViewModel>(
        converter: HomePageViewModel.fromStore,
        builder: (BuildContext context, HomePageViewModel viewModel) {
          return LoaderWidget(
            child: Scaffold(
              appBar: AppBar(
                title: Text(widget.title),
              ),
              body: Center(
                child: Text('Hello'),
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: viewModel.startLoader,
                tooltip: 'Increment',
                child: Icon(Icons.add),
              ), // This trailing comma makes auto-formatting nicer for build methods.
            ),
          );
        }
    );
  }
}