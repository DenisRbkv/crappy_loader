import 'package:flutter/foundation.dart';
import 'app_state.dart';
import 'loader_selectors.dart';
import 'package:redux/redux.dart';

class HomePageViewModel {
  final bool showLoader;
  final void Function() startLoader;
//  final void Function() endLoader;

  HomePageViewModel({
    @required this.showLoader,
    @required this.startLoader,
//    @required this.endLoader,
  });

  static HomePageViewModel fromStore(Store<AppState> store) {
    return HomePageViewModel(
      showLoader: AppStateSelectors.getLoaderValue(store),
      startLoader: AppStateSelectors.getStartLoaderFunction(store),
//      endLoader: AppStateSelectors.getEndLoaderFunction(store),
    );
  }
}
