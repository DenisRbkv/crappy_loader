import 'package:meta/meta.dart';
import 'loader_selectors.dart';
import 'loader_actions.dart';

@immutable
class AppState {
  final bool showLoader;

  AppState({this.showLoader});

  factory AppState.initial() => AppState(
        showLoader: false,
      );

  static AppState reducer(AppState state, dynamic action) {
    switch (action) {
      case StartLoader:
        return AppState(showLoader: !state.showLoader);
      default:
        return action;
    }
  }
}
