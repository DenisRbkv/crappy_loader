import 'app_state.dart';
import 'loader_actions.dart';
import 'package:redux/redux.dart';

class AppStateSelectors {

  static void Function() getStartLoaderFunction(Store<AppState> store) {
    return () => store.dispatch(StartLoader);
  }

//  static void Function() getEndLoaderFunction(Store<AppState> store) {
//    return () => store.dispatch(EndLoader);
//  }

  static bool getLoaderValue(Store<AppState> store) {
    return store.state.showLoader;
  }

}